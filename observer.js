const chokidar = require('chokidar')
const exec = require('child_process').exec
const paths = require('path')
const log4js = require('log4js')

log4js.configure({
  appenders: {
    api: { type: 'file', filename: './logs/api.log' },
    docker: { type: 'file',filename: './logs/docker.log' }
  },
  categories: {
    default: { appenders: ['api'], level: 'error' },
    docker: { appenders: ['docker'], level: 'debug' }
  }
});

const logger = log4js.getLogger('api')

let log = console.log.bind(console)

let watcher = chokidar.watch('./virtualhosts/**/docker-compose.yml',{
  ignored:/[\/\\]\./,
  persistent:true
})

watcher.on('ready',()=>{
  watcher.on('add', (path) => {
    var holder = paths.dirname(path)
    var command = `cd ${holder} && docker-compose up -d`
    exec(command, (err,stdout,stderr) => {
      if(err !== null){
        //エラー時の処理
         return logger.error(err)
      }
      
    })
  })

  watcher.on('change',(path) => {
    // TODO: ファイルを変更した際のメソッドをつくる
  })

  watcher.on('unlink',(path) => {
    // TODO: 削除した際のメソッドをつくる
  })

})
