#!/usr/bin/env python
# -*- coding: utf-8 -*-

import docker
import subprocess
client = docker.from_env()
for container in client.containers.list():
    print(container.name)
