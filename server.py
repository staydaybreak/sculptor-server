#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bottle import route, run, template,default_app,hook,response,HTTPResponse
import docker,json,os
import subprocess

@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

@route('/')
def index():
    return 'hello'

@route('/status',method="GET")
def status():
    client = docker.from_env()
    ret = []
    for container in client.containers.list():
        id,name,status = container.id,container.name,container.status
        container_object = {
            "id": id,
            "name": name,
            "staus": status
        }
        ret.append(container_object)

    body = json.dumps(ret)
    res = HTTPResponse(status=200,body=body)
    res.set_header('Content-Type', 'application/json')
    return res
@route('/up')
def up_container():
    workdir = '/Users/motomura/Docker/Spriggan/chirdren'
    os.chdir(workdir)
    r = subprocess.call('docker-compose up -d',shell=True)
    return r

app = default_app()

run(host="localhost",port=8000,debug=True,reloader=True)

